--create database
CREATE DATABASE KelasBE;

--create table
CREATE TABLE kelasbe (
	id INT,
	nama VARCHAR(20),
	tema VARCHAR(20),
	alamat VARCHAR(20),
	PRIMARY KEY (ID)
);

--insert data
INSERT INTO kelasbe (id, nama, tema, alamat)
VALUES (1, 'Ada', 'Data Type', 'Jakarta');
INSERT INTO kelasbe (id, nama, tema, alamat)
VALUES (2, 'Adi', 'OOP', 'Bogor');
INSERT INTO kelasbe (id, nama, tema, alamat)
VALUES (3, 'Adu', 'SQL', 'Depok');
INSERT INTO kelasbe (id, nama, tema, alamat)
VALUES (4, 'Ade', 'NoSQL', 'Tangerang');
INSERT INTO kelasbe (id, nama, tema, alamat)
VALUES (5, 'Ado', 'Data Type', 'Bekasi');

SELECT * FROM kelasbe;

--Tampilkan data berdasarkan id besar dari 3
SELECT * FROM kelasbe WHERE id > 3;

--Ubah nama dengan “nama telah diubah” berdasarkan id =3
UPDATE kelasbe
SET nama = 'nama telah diubah'
WHERE id = 3;

SELECT * FROM kelasbe;

--Tampilkan nama yang dimulai dari huruf ‘a’
SELECT * FROM kelasbe WHERE LOWER(nama) LIKE 'a%';

--Tampilkan data berdasarkan alamat , urutkan menggukan descending
SELECT * FROM kelasbe ORDER BY alamat DESC;

--Tampilkan data dengan mengelompokan berdasarkan tema
SELECT tema, COUNT(*) FROM kelasbe GROUP BY tema;

--Tampilkan data berdasarkan id = 3 or id = 4
SELECT * FROM kelasbe WHERE id = 3 or id = 4;